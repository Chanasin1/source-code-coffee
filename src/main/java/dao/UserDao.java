/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import Database.Database;
import Database.Database2;
import Ekarat.UserManagement2;
import com.mycompany.sourcecodecoffee.poc.TestSelectProduct;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
//import javafx.scene.control.ComboBox;
import javax.swing.JComboBox;
import javax.swing.JOptionPane;
import model.Customer;
import model.Product;
import model.User;

/**
 *
 * @author ekara
 */

public class UserDao {
    PreparedStatement pst = null;
    ResultSet rs = null;
    
    private static ArrayList<User> list = new ArrayList();

    
    public String add(User object) {
        Connection conn = null;
        
        conn = Database2.connect();
     String sql = "INSERT INTO USER(USER_USERNAME,USER_PASSWORD,USER_TYPE,EMP_ID) VALUES(?,?,?,?)";
        try {
            pst = conn.prepareStatement(sql);
            pst.setString(1, object.getUsername());
            pst.setString(2,object.getPassword());
            pst.setString(3,object.getType());
            pst.setString(4,object.getEmp_id());
            pst.execute();
            JOptionPane.showMessageDialog(null, "Insert Sucess");
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null,e);
        }
       Database2.close();
        return "";

    }
    public String delete(User object){
         Connection conn = null;
        conn = Database2.connect();
         try {
            String sql = "DELETE FROM USER WHERE USER_ID = ?";
            PreparedStatement stmt = conn.prepareStatement(sql);

            stmt.setString(1,object.getId());
            int row = stmt.executeUpdate();
            JOptionPane.showMessageDialog(null, "Delete Success");

            System.out.println("Affect row " + row);

        } catch (SQLException ex) {
            Logger.getLogger(UserManagement2.class.getName()).log(Level.SEVERE, null, ex);
        }Database2.close();
        return "";
    }
    
    public String update(User object){
        Connection conn = null;
        conn = Database2.connect();
          try {
            
            String sql = "UPDATE USER SET USER_USERNAME = ?,USER_PASSWORD = ?,USER_TYPE = ?,EMP_ID = ? WHERE USER_ID = ?";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, object.getUsername());
            stmt.setString(2, object.getPassword());
            stmt.setString(3, object.getType());
            stmt.setString(4, object.getEmp_id());
            stmt.setString(5, object.getId());
            int row = stmt.executeUpdate();
            JOptionPane.showMessageDialog(null, "Edit Success");
            System.out.println("Affect row " + row);

        } catch (SQLException ex) {
            Logger.getLogger(UserManagement2.class.getName()).log(Level.SEVERE, null, ex);
        }Database2.close();

        return "";
    }
     public ArrayList<User> getAll() {
        ArrayList list = new ArrayList();
        Connection conn = null;;
        conn = Database2.connect();
        try {
            String sql = "SELECT USER_ID,\n" +
"       USER_USERNAME,\n" +
"       USER_PASSWORD,\n" +
"       USER_TYPE,\n" +
"       EMP_ID\n" +
"  FROM USER;";
            Statement stmt = conn.createStatement();
            ResultSet resul = stmt.executeQuery(sql);
            while (resul.next()) {
               String id = resul.getString("USER_ID");
                String username = resul.getString("USER_USERNAME");
                String password = resul.getString("USER_PASSWORD");
                String type = resul.getString("USER_TYPE");
                String empid = resul.getString("EMP_ID");
                User user2 = new User(id, username,password, type,empid);
                list.add(user2);
            }
        } catch (SQLException ex) {
            Logger.getLogger(TestSelectProduct.class.getName()).log(Level.SEVERE, null, ex);
        } Database2.close();

      
        return list;
    }
     public String add2(Customer object){
         Connection conn = null;
          String sql = "INSERT INTO  CUSTOMER(CUS_NAME,CUS_TEL) VALUES(?,?)";
        try {
            
            pst = conn.prepareStatement(sql);
            pst.setString(1, object.getName());
            pst.setString(2,object.getTel());
            pst.execute();
            JOptionPane.showMessageDialog(null, "Insert Sucess");
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null,e);
        }return "";
     }
      public static boolean auth(String loginName,String password){
//          Connection conn = null;
//         try {
//                String sql = "SELECT USER_ID,\n"
//                        + "       USER_USERNAME,\n"
//                        + "       USER_PASSWORD,\n"
//                        + "       USER_TYPE,\n"
//                        + "       EMP_ID\n"
//                        + "  FROM USER;";
//                Statement stmt = conn.createStatement();
//                ResultSet result = stmt.executeQuery(sql);
//                
//                while (result.next()) {
//                    String id = result.getString("USER_ID");
//                    String username2 = result.getString("USER_USERNAME");
//                    String password2 = result.getString("USER_PASSWORD");
//                    String type = result.getString("USER_TYPE");
//                    String emp_id = result.getString("EMP_ID");
//                    User product = null;
//                    product = new User(id,username2,password2,type,emp_id);
//                    Syqstem.out.println(product);
//                }
//            } catch (SQLException ex) {
//                Logger.getLogger(Login.class.getName()).log(Level.SEVERE, null, ex);
//            }
        UserDao dao = new UserDao();
        ArrayList<User> newList = dao.getAll();
       
        list.addAll(newList);
         System.out.println(list);
        for(User user:list){
            
            if(loginName.equals(user.getUsername()) && password.equals(user.getPassword()))return true;
        }
        return false;
    }
      public String selectCombo(JComboBox a){
           Connection conn = Database2.connect();
         try{
             String sql = "SELECT EMP_ID,\n" +
"       EMP_FNAME,\n" +
"       EMP_LNAME,\n" +
"       EMP_ID_CARD,\n" +
"       EMP_TEL,\n" +
"       EMP_JOB_TYPE\n" +
"  FROM EMPLOYEE;";
             pst = conn.prepareStatement(sql);
             rs = pst.executeQuery();
             while(rs.next()){
                 String empid = rs.getString("EMP_ID");
                 a.addItem(empid);
             }
             
         }catch(Exception e){
             JOptionPane.showMessageDialog(null, e);
         }Database2.close();
         return "";
      }
    
    
}
