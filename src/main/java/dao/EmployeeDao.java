/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import com.mycompany.sourcecodecoffee.poc.TestSelectEmp;
import Database.Database;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Employee;

/**
 *
 * @author gaiga
 */
public class EmployeeDao implements DaoInterface<Employee> {

    @Override
    public int add(Employee object) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        int id = -1;
        try {
            String sql = "INSERT INTO EMPLOYEE (EMP_FNAME,EMP_LNAME,EMP_ID_CARD,EMP_TEL,EMP_JOB_TYPE) VALUES (?,?,?,?,?)";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, object.getFname());
            stmt.setString(2, object.getLname());
            stmt.setString(3, object.getIdCard());
            stmt.setString(4, object.getTel());
            stmt.setString(5, object.getJobType());
            int row = stmt.executeUpdate();
            ResultSet result = stmt.getGeneratedKeys();
            if (result.next()) {
                id = result.getInt(1);
            }
        } catch (SQLException ex) {
            Logger.getLogger(TestSelectEmp.class.getName()).log(Level.SEVERE, null, ex);
        }
        db.close();
        return id;
    }

    @Override
    public ArrayList<Employee> getAll() {
        ArrayList list = new ArrayList();
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        try {
            String sql = "SELECT EMP_ID,EMP_FNAME,EMP_LNAME,EMP_ID_CARD,EMP_TEL,EMP_JOB_TYPE FROM EMPLOYEE";
            Statement stmt = conn.createStatement();
            ResultSet resul = stmt.executeQuery(sql);
            while (resul.next()) {
                int id = resul.getInt("EMP_ID");
                String fname = resul.getString("EMP_FNAME");
                String lastname = resul.getString("EMP_LNAME");
                String idcard = resul.getString("EMP_ID_CARD");
                String tel = resul.getString("EMP_TEL");
                String jobtype = resul.getString("EMP_JOB_TYPE");
                Employee emp = new Employee(id, fname, lastname, idcard,tel,jobtype);
                list.add(emp);
            }
        } catch (SQLException ex) {
            Logger.getLogger(TestSelectEmp.class.getName()).log(Level.SEVERE, null, ex);
        }

        db.close();
        return list;
    }
    public ArrayList<Employee> getEmp(String id) {
        ArrayList list = new ArrayList();
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        try {
            String sql = "SELECT EMP_ID,EMP_FNAME,EMP_LNAME,EMP_ID_CARD,EMP_TEL,EMP_JOB_TYPE FROM EMPLOYEE WHERE EMP_ID='" + id+"'";
            Statement stmt = conn.createStatement();
            ResultSet resul = stmt.executeQuery(sql);
            while (resul.next()) {
                int eid = resul.getInt("EMP_ID");
                String fname = resul.getString("EMP_FNAME");
                String lastname = resul.getString("EMP_LNAME");
                String idcard = resul.getString("EMP_ID_CARD");
                String tel = resul.getString("EMP_TEL");
                String jobtype = resul.getString("EMP_JOB_TYPE");
                Employee emp = new Employee(eid, fname, lastname, idcard,tel,jobtype);
                list.add(emp);
            }
        } catch (SQLException ex) {
            Logger.getLogger(TestSelectEmp.class.getName()).log(Level.SEVERE, null, ex);
        }

        db.close();
        return list;
    }
   

    @Override
    public Employee get(int id) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        try {
            String sql = "SELECT * FROM EMPLOYEE WHERE EMP_ID=" + id;
            Statement stmt = conn.createStatement();
            ResultSet resul = stmt.executeQuery(sql);
            if (resul.next()) {
                int cid = resul.getInt("EMP_ID");
                String fname = resul.getString("EMP_FNAME");
                String lname = resul.getString("EMP_LNAME");
                String idcard = resul.getString("EMP_ID_CARD");
                String tel = resul.getString("EMP_TEL");
                String jobtype = resul.getString("EMP_JOB_TYPE");
                Employee emp = new Employee(cid, fname, lname, idcard,tel,jobtype);
                return emp;
            }
        } catch (SQLException ex) {
            Logger.getLogger(TestSelectEmp.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    

    @Override
    public int delete(int id) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        int row = 0;
        try {
            String sql = "DELETE FROM EMPLOYEE WHERE EMP_ID = ?";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            row = stmt.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(TestSelectEmp.class.getName()).log(Level.SEVERE, null, ex);
        }

        db.close();
        return row;
    }

    @Override
    public int update(Employee object) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        int row = 0;
        try {
            String sql = "UPDATE EMPLOYEE SET EMP_FNAME = ?,EMP_LNAME = ? ,EMP_ID_CARD = ? ,EMP_TEL = ? ,EMP_JOB_TYPE = ? WHERE EMP_ID = ?";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, object.getFname());
            stmt.setString(2, object.getLname());
            stmt.setString(3, object.getIdCard());
            stmt.setString(4, object.getTel());
            stmt.setString(5, object.getJobType());
            stmt.setInt(6, object.getId());
            row = stmt.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(TestSelectEmp.class.getName()).log(Level.SEVERE, null, ex);
        }
        db.close();
        return row;    
    }

    
     
    
}
