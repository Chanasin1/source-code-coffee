/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import Database.Database;
import com.mycompany.sourcecodecoffee.poc.TestSelectProduct;
import model.Product;

/**
 *
 * @author User
 */
public class ProductDao implements DaoInterface<Product>{

    @Override
    public int add(Product object) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        int id = -1;
        try {
            String sql = "INSERT INTO PRODUCT (PRODUCT_NAME, PRODUCT_PRICE, PRODUCT_AMOUNT) VALUES (?, ?, ?)";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, object.getName());
            stmt.setDouble(2, object.getPrice());
            stmt.setInt(3, object.getAmount());
            int row = stmt.executeUpdate();
            ResultSet result = stmt.getGeneratedKeys();
            if(result.next()) {
                id = result.getInt(1);
            }
        } catch (SQLException ex) {
            System.out.println("Error : to create product!!");
        }
        db.close();
        return id;
    }

    @Override
    public ArrayList<Product> getAll() {
        ArrayList list = new ArrayList();
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        try {
            String sql = "SELECT PRODUCT_ID,\n" +
"       PRODUCT_NAME,\n" +
"       PRODUCT_PRICE,\n" +
"       PRODUCT_AMOUNT\n" +
"  FROM PRODUCT;";
            Statement stmt = conn.createStatement();
            ResultSet result = stmt.executeQuery(sql);
            while (result.next()) {
                int id = result.getInt("PRODUCT_ID");
                String name = result.getString("PRODUCT_NAME");
                double price = result.getDouble("PRODUCT_PRICE");
                int amount = result.getInt("PRODUCT_AMOUNT");
                Product product = new Product(id, name, price, amount);
                list.add(product);
                System.out.println(product);
            }
        } catch (SQLException ex) {
           System.out.println("Error : Unable to select all product!! " + ex.getMessage());
        }
        db.close();
        return list;
    }
    public ArrayList<Product> getId(String id) {
        ArrayList list = new ArrayList();
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        try {
            String sql = "SELECT PRODUCT_ID, PRODUCT_NAME, PRODUCT_PRICE, PRODUCT_AMOUNT FROM PRODUCT WHERE PRODUCT_ID='" + id+"'";
            Statement stmt = conn.createStatement();
            ResultSet ans = stmt.executeQuery(sql);
            while (ans.next()) {
                int pid = ans.getInt("PRODUCT_ID");
                String name = ans.getString("PRODUCT_NAME");
                double price = ans.getDouble("PRODUCT_PRICE");
                int amount = ans.getInt("PRODUCT_AMOUNT");
                Product product = new Product(pid, name, price, amount);
                list.add(product);
                
            }
        } catch (SQLException ex) {
            System.out.println("Error : Unable to select product id " + id + "!!");
        }
        db.close();
        return list;
    }   

     public ArrayList<Product> getMin() {
        ArrayList list = new ArrayList();
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        try {
            String sql = "SELECT * FROM PRODUCT WHERE PRODUCT_AMOUNT <=5";
            Statement stmt = conn.createStatement();
            ResultSet result = stmt.executeQuery(sql);
            while (result.next()) {
                int id = result.getInt("PRODUCT_ID");
                String name = result.getString("PRODUCT_NAME");
                double price = result.getDouble("PRODUCT_PRICE");
                int amount = result.getInt("PRODUCT_AMOUNT");
                Product product = new Product(id, name, price, amount);
                list.add(product);
                System.out.println(product);
            }
        } catch (SQLException ex) {
            System.out.println("Error : Unable to select all product!! " + ex.getMessage());
        }
        db.close();
        return list;
    }

    
    
    public ArrayList<Product> getStock(String id) {
        ArrayList list = new ArrayList();
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        try { 
            
          //  String sql2 = " SELECT PRODUCT_AMOUNT FROM PRODUCT ORDER BY PRODUCT_AMOUNT DESC;";
            String sql = "SELECT PRODUCT_ID, PRODUCT_NAME, PRODUCT_PRICE, PRODUCT_AMOUNT FROM PRODUCT WHERE PRODUCT_ID ORDER BY PRODUCT_AMOUNT DESC='" + id+"'";
            Statement stmt = conn.createStatement();
     //        ResultSet result2 = stmt.executeQuery(sql2);
            ResultSet result = stmt.executeQuery(sql);
            while (result.next()) {
                int sid = result.getInt("PRODUCT_ID");
                String name = result.getString("PRODUCT_NAME");
                double price = result.getDouble("PRODUCT_PRICE");
                int amount = result.getInt("PRODUCT_AMOUNT");
                Product product = new Product(sid, name, price, amount);
                list.add(product);
                System.out.println(product);
            }
        } catch (SQLException ex) {
           System.out.println("Error : Unable to select all product!! " + ex.getMessage());
        }
        db.close();
        return list;
    }

    @Override
    public Product get(int id) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        try {
            String sql = "SELECT PRODUCT_ID, PRODUCT_NAME, PRODUCT_PRICE, PRODUCT_AMOUNT FROM PRODUCT WHERE PRODUCT_ID=" + id;
            Statement stmt = conn.createStatement();
            ResultSet result = stmt.executeQuery(sql);
            if(result.next()) {
                int pid = result.getInt("PRODUCT_ID");
                String name = result.getString("PRODUCT_NAME");
                double price = result.getDouble("PRODUCT_PRICE");
                int amount = result.getInt("PRODUCT_AMOUNT");
                Product product = new Product(pid, name, price,amount);
                return product;
            }
        } catch (SQLException ex) {
            System.out.println("Error : Unable to select product id " + id + "!!" + ex.getMessage());
        }
        return null;
    }

    

    
    public ArrayList<Product> getName(String name) {
        ArrayList list = new ArrayList();
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        
        try {
            
            String sql = "SELECT * FROM PRODUCT WHERE PRODUCT_NAME = \'"+name+"\'";
            Statement stmt = conn.createStatement();
            ResultSet ans = stmt.executeQuery(sql);
            if (ans.next()) {
                
                int id = ans.getInt("PRODUCT_ID");
                String pname = ans.getString("PRODUCT_NAME");
                double price = ans.getDouble("PRODUCT_PRICE");
                int amount = ans.getInt("PRODUCT_AMOUNT");
                Product product = new Product(id, pname, price, amount);
                list.add(product);
                
            }
        } catch (SQLException ex) {
            System.out.println("Error : Unable to select product name " + name + "!!");
        }
        db.close();
        return list;
    }

    @Override
    public int delete(int id) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        int row = 0;

        try {
            String sql = "DELETE FROM PRODUCT WHERE PRODUCT_ID = ?";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            row =stmt.executeUpdate();
        } catch (SQLException ex) {
            System.out.println("Error : Unable to delete product id " + id + "!!");
        }

        db.close();
        return row;
    }

    @Override
    public int update(Product object) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        int row = 0;
        try {
            String sql = "UPDATE PRODUCT SET PRODUCT_NAME = ?, PRODUCT_PRICE = ?, PRODUCT_AMOUNT = ? WHERE PRODUCT_ID = ?";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, object.getName());
            stmt.setDouble(2, object.getPrice());
            stmt.setInt(3, object.getAmount());
            stmt.setInt(4, object.getId());
            row = stmt.executeUpdate();
        } catch (SQLException ex) {
            System.out.println("Error : Unable to update product " + object + "!!");
        }

        db.close();
        return row;
    }
}
