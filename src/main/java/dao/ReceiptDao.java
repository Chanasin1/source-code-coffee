/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import Database.Database;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Customer;
import model.Employee;
import model.Product;
import model.Receipt;
import model.ReceiptDetail;
import model.User;

/**
 *
 * @author Windows10
 */
public class ReceiptDao implements DaoInterface<Receipt> {

    @Override
    public int add(Receipt object) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        int id = -1;
        try {
            String sql = "INSERT INTO RECIEPT (RCPT_TOTALPRICE, CUS_ID, EMP_ID) VALUES (?, ?, ?)";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setDouble(1, object.getTotal());
            stmt.setInt(2, object.getCustomer().getId());
            stmt.setInt(3, object.getSeller().getId());
            int row = stmt.executeUpdate();
            ResultSet result = stmt.getGeneratedKeys();
            if (result.next()) {
                id = result.getInt(1);
                object.setId(id);
            }
            for (ReceiptDetail r : object.getReceiptDetail()) {
                String sqlDetail = "INSERT INTO RECIEPT_DETAIL (RECEIPT_DETAIL_AMOUNT, RECEIPT_DETAIL_PRICE, RCPT_ID, PRODUCT_ID) VALUES (?, ?, ?, ?)";
                PreparedStatement stmtDetail = conn.prepareStatement(sqlDetail);
                stmtDetail.setInt(1, r.getAmount());
                stmtDetail.setDouble(2, r.getPrice());
                stmtDetail.setInt(3, r.getRecceipt().getId());
                stmtDetail.setInt(4, r.getProduct().getId());
                int rowDetail = stmtDetail.executeUpdate();
                ResultSet resultDetail = stmt.getGeneratedKeys();
                if (resultDetail.next()) {
                    id = resultDetail.getInt(1);
                    r.setId(id);
                }
            }
        } catch (SQLException ex) {
            System.out.println("Error : to create receipt");
        }
        db.close();
        return id;
    }

    @Override
    public ArrayList<Receipt> getAll() {
        ArrayList list = new ArrayList();
        Connection conn = null;
        String dbPath = "./db/coffee.db";
        Database db = Database.getInstance();
        conn = db.getConnection();
        try {
            String sql = "SELECT RCPT_ID, RCPT_CREATED, "
                    + "c.CUS_ID, c.CUS_NAME,  c.CUS_TEL, c.CUS_POINT, "
                    + "e.EMP_ID, e.EMP_FNAME, e.EMP_LNAME, e.EMP_TEL, "
                    + "RCPT_TOTALPRICE "
                    + "FROM RECIEPT r, CUSTOMER c, EMPLOYEE e "
                    + "WHERE r.CUS_ID = c.CUS_ID AND r.EMP_ID = e.EMP_ID "
                    + "ORDER BY RCPT_CREATED DESC";
            Statement stmt = conn.createStatement();
            ResultSet result = stmt.executeQuery(sql);
            while (result.next()) {
                int id = result.getInt("RCPT_ID");
                Date created = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(result.getString("RCPT_CREATED"));
                int customerId = result.getInt("CUS_ID");
                String customerName = result.getString("CUS_NAME");
                String customerTel = result.getString("CUS_TEL");
                int customerPoint = result.getInt("CUS_POINT");
                int empId = result.getInt("EMP_ID");
                String empFName = result.getString("EMP_FNAME");
                String empLName = result.getString("EMP_LNAME");
                String empTel = result.getString("EMP_TEL");
                double total = result.getDouble("RCPT_TOTALPRICE");
                Receipt receipt = new Receipt(id, created,
                        new Employee(empId, empFName, empLName, empTel),
                        new Customer(customerId, customerName, customerTel, customerPoint)
                );
                list.add(receipt);
            }
        } catch (SQLException ex) {
            System.out.println("Error : Unable to select all receipt!! " + ex.getMessage());
        } catch (ParseException ex) {
            System.out.println("Error : Date parsing all receipt!! " + ex.getMessage());
        }
        db.close();
        return list;
    }
    
    public ArrayList<Receipt> getReceipt() {
        ArrayList list = new ArrayList();
        Connection conn = null;
        String dbPath = "./db/coffee.db";
        Database db = Database.getInstance();
        conn = db.getConnection();
        try {
            String sql = "SELECT RCPT_ID, RCPT_CREATED, "
                    + "c.CUS_ID, c.CUS_NAME, c.CUS_TEL, c.CUS_POINT, "
                    + "e.EMP_ID, e.EMP_FNAME, e.EMP_LNAME, e.EMP_TEL, "
                    + "RCPT_TOTALPRICE "
                    + "FROM RECIEPT r, CUSTOMER c, EMPLOYEE e  "
                    + "WHERE RCPT_ID = 2 AND r.CUS_ID = c.CUS_ID AND r.EMP_ID = e.EMP_ID";
            Statement stmt = conn.createStatement();
            ResultSet result = stmt.executeQuery(sql);
            while (result.next()) {
                int id = result.getInt("RCPT_ID");
                Date created = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(result.getString("RCPT_CREATED"));
                int customerId = result.getInt("CUS_ID");
                String customerName = result.getString("CUS_NAME");
                String customerTel = result.getString("CUS_TEL");
                int customerPoint = result.getInt("CUS_POINT");
                int empId = result.getInt("EMP_ID");
                String empFName = result.getString("EMP_FNAME");
                String empLName = result.getString("EMP_LNAME");
                String empTel = result.getString("EMP_TEL");
                double total = result.getDouble("RCPT_TOTALPRICE");
                Receipt receipt = new Receipt(id, created,
                        new Employee(empId, empFName, empLName, empTel),
                        new Customer(customerId, customerName, customerTel, customerPoint)
                );
                list.add(receipt);
            }
        } catch (SQLException ex) {
            System.out.println("Error : Unable to select all receipt!! " + ex.getMessage());
        } catch (ParseException ex) {
            System.out.println("Error : Date parsing all receipt!! " + ex.getMessage());
        }
        db.close();
        return list;
    }

    @Override
    public Receipt get(int id) {
        Connection conn = null;
        String dbPath = "./db/coffee.db";
        Database db = Database.getInstance();
        conn = db.getConnection();
        try {
            String sql = "SELECT RCPT_ID, RCPT_CREATED, "
                    + "c.CUS_ID, c.CUS_NAME, c.CUS_TEL, c.CUS_POINT, "
                    + "e.EMP_ID, e.EMP_FNAME, e.EMP_LNAME, e.EMP_TEL, "
                    + "RCPT_TOTALPRICE "
                    + "FROM RECIEPT r, CUSTOMER c, EMPLOYEE e  "
                    + "WHERE RCPT_ID = ? AND r.CUS_ID = c.CUS_ID AND r.EMP_ID = e.EMP_ID";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet result = stmt.executeQuery();
            if (result.next()) {
                int rid = result.getInt("RCPT_ID");
                Date created = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(result.getString("RCPT_CREATED"));
                int customerId = result.getInt("CUS_ID");
                String customerName = result.getString("CUS_NAME");
                String customerTel = result.getString("CUS_TEL");
                int customerPoint = result.getInt("CUS_POINT");
                int empId = result.getInt("EMP_ID");
                String empFName = result.getString("EMP_FNAME");
                String empLName = result.getString("EMP_LNAME");
                String empTel = result.getString("EMP_TEL");
                double total = result.getDouble("RCPT_TOTALPRICE");
                Receipt receipt = new Receipt(rid, created,
                        new Employee(empId, empFName, empLName, empTel),
                        new Customer(customerId, customerName, customerTel, customerPoint)
                );
                getReceiptDetail(conn, id, receipt);
                return receipt;
            }
        } catch (SQLException ex) {
            System.out.println("Error : Unable to select receipt id " + id + "!!" + ex.getMessage());
        } catch (ParseException ex) {
            System.out.println("Error : Date parsing all receipt!! " + ex.getMessage());
        }
        return null;
    }

    
    private void getReceiptDetail(Connection conn, int id, Receipt receipt) throws SQLException {
        String sqlDetail = "SELECT RECIEPT_DETAIL_ID, "
                + "RCPT_ID, "
                + "p.PRODUCT_ID, p.PRODUCT_NAME , p.PRODUCT_PRICE, p.PRODUCT_AMOUNT, "
                + "RECEIPT_DETAIL_PRICE, RECEIPT_DETAIL_AMOUNT "
                + "FROM RECIEPT_DETAIL rd, PRODUCT p "
                + "WHERE RCPT_ID = ? AND rd.PRODUCT_ID = p.PRODUCT_ID";
        PreparedStatement stmtDetail = conn.prepareStatement(sqlDetail);
        stmtDetail.setInt(1, id);
        ResultSet resultDetail = stmtDetail.executeQuery();
        while (resultDetail.next()) {
            int receiveId = resultDetail.getInt("RECIEPT_DETAIL_ID");
            int productId = resultDetail.getInt("PRODUCT_ID");
            String productName = resultDetail.getString("PRODUCT_NAME");
            double productPrice = resultDetail.getDouble("PRODUCT_PRICE");
            int productAmount = resultDetail.getInt("PRODUCT_AMOUNT");
            double price = resultDetail.getDouble("RECEIPT_DETAIL_PRICE");
            int amount = resultDetail.getInt("RECEIPT_DETAIL_AMOUNT");
            Product product = new Product(productId, productName, productPrice, productAmount);
            receipt.addReceiptDetail(receiveId, amount, price, product);
        }
    }

    @Override
    public int delete(int id) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        int row = 0;
        try {
            String sql = "DELETE FROM RECIEPT WHERE RCPT_ID = ?";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            row = stmt.executeUpdate();
        } catch (SQLException ex) {
            System.out.println("Error : Unable to delete receipt id " + id + "!!");
        }
        db.close();
        return row;
    }

    @Override
    public int update(Receipt object) {
        /* Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        int row = 0;
        try {
            String sql = "UPDATE Receipt SET name = ?, price = ? WHERE id = ?";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, object.getName());
            stmt.setDouble(2, object.getPrice());
            stmt.setInt(3, object.getId());
            row = stmt.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(TestSelectReceipt.class.getName()).log(Level.SEVERE, null, ex);
        }
        db.close(); */
        return 0;
    }

    public static void main(String[] args) {
        Product p1 = new Product(1, "Americano", 30, 10);
        Product p2 = new Product(2, "Espresso", 30, 9);
        Employee seller = new Employee(1, "Chanasin", "Choetkaipetchara", "1234567890123", "0848694479", "Part-Time");
        Customer customer = new Customer(1, "Ake", "0972311021", 20);
        Receipt receipt = new Receipt(seller, customer);
        receipt.addReceiptDetail(1, p1);
        receipt.addReceiptDetail(3, p2);
        System.out.println(receipt);
        ReceiptDao dao = new ReceiptDao();
        System.out.println("id = " + dao.add(receipt));
        System.out.println("Receipt after add : " + receipt);
        System.out.println("Get all : " + dao.getAll());

        Receipt newReceipt = dao.get(receipt.getId());
        System.out.println("New Receipt : " + newReceipt);

        //dao.delete(receipt.getId());
    }
}
