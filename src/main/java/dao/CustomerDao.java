/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import com.mycompany.sourcecodecoffee.poc.TestSelectCustomer;
import Database.Database;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Customer;

/**
 *
 * @author Taneat
 */
public class CustomerDao implements DaoInterface<Customer> {

    @Override
    public int add(Customer object) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        int id = -1;
        try {
            String sql = "INSERT INTO CUSTOMER (CUS_NAME,CUS_TEL,CUS_POINT) VALUES (?,?,?)";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, object.getName());
            stmt.setString(2, object.getTel());
            stmt.setInt(3, object.getPoint());
            int row = stmt.executeUpdate();
            ResultSet result = stmt.getGeneratedKeys();
            if (result.next()) {
                id = result.getInt(1);
            }
        } catch (SQLException ex) {
            Logger.getLogger(TestSelectCustomer.class.getName()).log(Level.SEVERE, null, ex);
        }
        db.close();
        return id;
    }

    @Override
    public ArrayList<Customer> getAll() {
        ArrayList list = new ArrayList();
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        try {
            String sql = "SELECT CUS_ID, CUS_NAME, CUS_TEL ,CUS_POINT FROM CUSTOMER";
            Statement stmt = conn.createStatement();
            ResultSet resul = stmt.executeQuery(sql);
            while (resul.next()) {
                int id = resul.getInt("CUS_ID");
                String name = resul.getString("CUS_NAME");
                String tel = resul.getString("CUS_TEL");
                int point = resul.getInt("CUS_POINT");
                Customer customer = new Customer(id, name, tel,point);
                list.add(customer);
            }
        } catch (SQLException ex) {
            Logger.getLogger(TestSelectCustomer.class.getName()).log(Level.SEVERE, null, ex);
        }

        db.close();
        return list;
    }

    public ArrayList<Customer> getCus(String tel) {
        ArrayList list = new ArrayList();
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        try {
            String sql = "SELECT CUS_ID, CUS_NAME, CUS_TEL, CUS_POINT FROM CUSTOMER WHERE CUS_TEL='" + tel+"'";
            Statement stmt = conn.createStatement();
            ResultSet resul = stmt.executeQuery(sql);
            if (resul.next()) {
                int id = resul.getInt("CUS_ID");
                String name = resul.getString("CUS_NAME");
                String tell = resul.getString("CUS_TEL");
                int point = resul.getInt("CUS_POINT");
                Customer customer = new Customer(id, name, tell,point);
                list.add(customer);
            }
        } catch (SQLException ex) {
            Logger.getLogger(TestSelectCustomer.class.getName()).log(Level.SEVERE, null, ex);
        }
        db.close();
        return list;
    }
    @Override
    public int delete(int id) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        int row = 0;
        try {
            String sql = "DELETE FROM CUSTOMER WHERE CUS_ID = ?";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            row = stmt.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(TestSelectCustomer.class.getName()).log(Level.SEVERE, null, ex);
        }

        db.close();
        return row;
    }

    @Override
    public int update(Customer object) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        int row = 0;
        try {
            String sql = "UPDATE CUSTOMER SET CUS_NAME = ?,CUS_TEL = ? ,CUS_POINT = ? WHERE CUS_ID = ?";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, object.getName());
            stmt.setString(2, object.getTel());
            stmt.setInt(3, object.getPoint());
            stmt.setInt(4, object.getId());
            row = stmt.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(TestSelectCustomer.class.getName()).log(Level.SEVERE, null, ex);
        }
        db.close();
        return row;    }

    public static void main(String[] args) {
        CustomerDao dao = new CustomerDao();
        System.out.println(dao.getAll());
        System.out.println(dao.get(1));
        int id = dao.add(new Customer(-1, "Anna", "0912223333",50));
        System.out.println("id: " + id);
        Customer lastCustomer = dao.get(id);
        System.out.println("last customer: "+lastCustomer);
        lastCustomer.setTel("0945555555");
        int row = dao.update(lastCustomer);
        Customer updateCustomer = dao.get(id);
        System.out.println("update customer: "+updateCustomer);
        dao.delete(id);
        Customer deleteCustomer = dao.get(id);
        System.out.println("delete customer: "+deleteCustomer);
    }

    @Override
    public Customer get(int id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
