/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author User
 */
public class ReceiptDetail {

    private int id;
    private int amount;
    private double price;
    private Product product;
    private Receipt receipt;

    public ReceiptDetail(int id, int amount, double price, Product product, Receipt receipt) {
        this.id = id;
        this.amount = amount;
        this.price = price;
        this.product = product;
        this.receipt = receipt;
    }

    public ReceiptDetail(int amount, double price, Product product, Receipt receipt) {
        this(-1, amount, price, product, receipt);
    }

    public double getTotal() {
        return amount * price;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public Receipt getRecceipt() {
        return receipt;
    }

    public void setRecceipt(Receipt recceipt) {
        this.receipt = recceipt;
    }

    public void addAmount(int amount) {
        this.amount = this.amount + amount;
    }

    @Override
    public String toString() {
        return "ReceiptDetail{" + "id=" + id
                + ", amount=" + amount
                + ", price=" + price
                + ", total=" + this.getTotal()
                + ", product=" + product
                + '}';
    }

}
