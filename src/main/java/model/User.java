/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author Kotori
 */
public class User {

    String id;
    String username;
    String password;
    String type;
    String emp_id;


  

    public User(String id,String username,String password,String type,String emp_id) {
        this.id = id;
        this.username = username;
        this.password = password;

        this.type = type;
        this.emp_id = emp_id;
        
    }

    public String getEmp_id() {
        return emp_id;
    }

    public void setEmp_id(String emp_id) {
        this.emp_id = emp_id;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    @Override
    public String toString() {
        return "User{" + "id=" + id + ", username=" + username + ", password=" + password + ", type=" + type + ", emp_id=" + emp_id + '}';
    }

    

   

    
  

    


    

}