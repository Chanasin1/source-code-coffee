/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author gaiga
 */
public class Employee {
    private int id;
    private String Fname;
    private String Lname;
    private String tel;
    private String idCard;
    private String jobType;

    public Employee(int id, String Fname,String Lname,String idCard,String tel,String jobType ) {
        this.id = id;
        this.Fname = Fname;
        this.Lname = Lname;
        this.idCard = idCard;
        this.tel = tel;
        this.jobType = jobType;   
    }
    
    public Employee(int id, String Fname,String Lname,String tel) {
        this.id = id;
        this.Fname = Fname;
        this.Lname = Lname;
        this.tel = tel;
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFname() {
        return Fname;
    }

    public void setFname(String Fname) {
        this.Fname = Fname;
    }

    public String getLname() {
        return Lname;
    }

    public void setLname(String Lname) {
        this.Lname = Lname;
    }

    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

    public String getIdCard() {
        return idCard;
    }

    public void setIdCard(String idCard) {
        this.idCard = idCard;
    }

    public String getJobType() {
        return jobType;
    }

    public void setJobType(String jobType) {
        this.jobType = jobType;
    }

    @Override
    public String toString() {
        return "Employee{" + "id=" + id + ", Fname=" + Fname + ", Lname=" + Lname + ", idCard=" + idCard + ", tel=" + tel + ", jobType=" + jobType +  '}';
    }
    
    
}
