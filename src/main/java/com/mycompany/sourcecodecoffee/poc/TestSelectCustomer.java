/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.sourcecodecoffee.poc;

import Database.Database;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Customer;

/**
 *
 * @author Taneat
 */
public class TestSelectCustomer {

    public static void main(String[] args) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        try {
            String sql = "SELECT CUS_ID, CUS_NAME, CUS_TEL, CUS_POINT FROM CUSTOMER";
            Statement stmt = conn.createStatement();
            ResultSet resul = stmt.executeQuery(sql);
            while (resul.next()) {
                int id = resul.getInt("CUS_ID");
                String name = resul.getString("CUS_NAME");
                String tel = resul.getString("CUS_TEL");
                int point = resul.getInt("CUS_POINT");
                Customer product =new Customer(id,name,tel,point);
                System.out.println(product);
            }
        } catch (SQLException ex) {
            Logger.getLogger(TestSelectCustomer.class.getName()).log(Level.SEVERE, null, ex);
        }

        db.close();
    }
}
