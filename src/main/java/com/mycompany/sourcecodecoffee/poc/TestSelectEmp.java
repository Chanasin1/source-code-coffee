/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.sourcecodecoffee.poc;

import Database.Database;
import java.sql.*;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Employee;

/**
 *
 * @author gaiga
 */
public class TestSelectEmp {

    public static void main(String[] args) {
        java.sql.Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();

        try {
            String sql = "SELECT * FROM EMPLOYEE";
            Statement stmt = conn.createStatement();
            ResultSet result = stmt.executeQuery(sql);
            while (result.next()) {
                int id = result.getInt("EMP_ID");
                String fname = result.getString("EMP_FNAME");
                String lname = result.getString("EMP_LNAME");
                String idcard = result.getString("EMP_ID_CARD");
                String tel = result.getString("EMP_TEL");
                String jobtype = result.getString("EMP_JOB_TYPE");
                Employee emp = new Employee(id, fname, lname, idcard, tel, jobtype);
                System.out.println(emp);
            }
        } catch (SQLException ex) {
            Logger.getLogger(TestSelectEmp.class.getName()).log(Level.SEVERE, null, ex);
        }
        db.close();
    }

}
